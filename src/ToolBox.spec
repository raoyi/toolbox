# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['ToolBox.py'],
    pathex=[],
    binaries=[],
    datas=[('img/http.webp', 'img/'), ('img/ping.webp', 'img/'), ('img/about.webp', 'img/'), ('img/update.webp', 'img/'), ('img/logo.webp', 'img/'), ('img/yi_logo.webp', 'img/')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
    optimize=0,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.datas,
    [],
    name='ToolBox',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    version='version.txt',
    icon=['img\\logo.ico'],
)
